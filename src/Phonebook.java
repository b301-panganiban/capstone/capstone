import java.util.ArrayList;
import java.util.List;

public class Phonebook {
    private List<Contact> contacts;

    // constructor
    public Phonebook() {
        contacts = new ArrayList<>();
    }

    public Phonebook(List<Contact> contacts) {
        this.contacts = contacts;
    }

    // getter & setter
    public List<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
    }

    // add contact
    public void addContact(Contact contact) {
        contacts.add(contact);
    }

    // check contact
    public boolean isEmpty() {
        return contacts.isEmpty();
    }
}