import java.util.List;

public class Main {
    public static void main(String[] args) {

        // instantiate new phonebook
        Phonebook phonebook = new Phonebook();

        // instantiate new contact
        Contact contact1 = new Contact("Kyrie Irving", "+69565400773", "Dasmarinas, Cavite");
        Contact contact2 = new Contact("LeBron James", "+69123456789", "Commonwealth, Quezon City");

        // add contacts
        phonebook.addContact(contact1);
        phonebook.addContact(contact2);


        // if phonebook empty
        if (phonebook.isEmpty()) {
            System.out.println("Phonebook is empty.");
        } else {
            List<Contact> contacts = phonebook.getContacts();
            for (Contact contact : contacts) {

                System.out.println(contact.getName());

                System.out.println("-------------------------");

                System.out.println(contact.getName() + " has the following registered number: ");
                System.out.println(contact.getContactNumber());

                System.out.println(contact.getName() + " has the following registered address: ");
                System.out.println(contact.getAddress());
                System.out.println(" ");
            }
        }
    }
}